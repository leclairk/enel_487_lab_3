/**
Filename: timer_impl_c.c
Author: Tessa Herzberger and Kiley LeClair
Class: enel487
Project: lab3
*/

/**
This file contains all of the addresses for the registers, as well as the setupRegs function prototype.
*/

#include "timer.h"


void timer_init(void)
{
	uint32_t TIM2_CR1_ARPE = 0x00000080;
	uint32_t TIM2_CR1_CEN = 0x00000001;

	
	* regRCC_APB1ENR |= 0x1; //Enable the clock for TIM2
//	GPIOA->CRH = 0xB;
	* regTIM2_CR1 |= 0x00000011; //Enable the counter and the DIR register to downcount
	* regTIM2_ERG |= 0x00000001; // Turn on the UG bit
	
	//Might not need this line
//	* regTIM2_CCMR1 |= 0x0000006C; //Turn on Output compare 1 preload enable and output compare 1 fast enable
	//Might not need this line
//	* regTIM2_CCER |= 0x00000001; //TUrn on the Capture enable (if input) or the OC1 signal turned to active (if output)
	* regTIM2_ARR = 0xFFFF; //Auto Reload register set to the maximum clock frequency, thus counter is not blocked
//	* regTIM2_CCR1 = 50; // 50%duty cycle
	* regTIM2_CR1 |= TIM2_CR1_ARPE | TIM2_CR1_CEN; //Enable the counter and the DIR register to downcount
	
	return;
}
	
int16_t timer_start(void)
{
	int16_t start_time = 0x0000;
	
	start_time = * regTIM2_CNT;
	
	return start_time;
}
int16_t timer_stop(int16_t start_time)
{	
	int16_t stop_time = 0x0000;

	stop_time = * regTIM2_CNT;
			
	return stop_time;
}

void timer_shutdown()
{
	uint32_t TIM2_CR1_ARPE = 0x00000080;
	uint32_t TIM2_CR1_CEN = 0x00000001;

	
	* regRCC_APB1ENR &= ~0x1; //Enable the clock for TIM2
//	GPIOA->CRH = 0xB;
	* regTIM2_CR1 &= ~0x00000011; //Enable the counter and the DIR register to downcount
	* regTIM2_ERG &= 0x00000001; // Turn on the UG bit
	
	//Might not need this line
//	* regTIM2_CCMR1 &= ~0x0000006C; //Turn on Output compare 1 preload enable and output compare 1 fast enable
	//Might not need this line
//	* regTIM2_CCER &= ~0x00000001; //TUrn on the Capture enable (if input) or the OC1 signal turned to active (if output)
	* regTIM2_ARR = 0x0000; //Auto Reload register set to the maximum clock frequency, thus counter is not blocked
//	* regTIM2_CCR1 = 50; // 50%duty cycle
	* regTIM2_CR1 &= ~(TIM2_CR1_ARPE | TIM2_CR1_CEN); //Enable the counter and the DIR register to downcount
	
	return;
}

int16_t timer_func(int16_t start_time, int16_t stop_time)
{
	uint32_t u_stop_time = 0x0;
	int16_t wrap_around = 0x0;

	start_time = timer_start();
	stop_time = timer_stop(start_time);
	
//		//If the counter fills up, reset it to zero.
//	//65535 = 0xFFFF
//	if (start_time == 0xFFFF)
//		start_time = 0x0000;
	
	if (stop_time > start_time)
	{
		wrap_around += 1;
		
		if (wrap_around != 0)
		{
			//convert stop time from negative to positive using twos compliment.
			u_stop_time = (~u_stop_time) + 1;
			//Add 65535 to acount for the wrap around.
			u_stop_time += 0xFFFF;
		}
	}
	
	return stop_time - start_time;	
}

