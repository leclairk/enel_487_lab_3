Author: Tessa Herzberger and Kiley LeClair
Class: enel487
Project: lab3


Filename: main.c
- The main function initializes the values of
  many variables, then determines what input the user selected.
  Finally, it resets everything.
- The main polls to see if anything has been typed and waits for an enter key.
-  It sends a character array to determine_user_input() which does most of the functionality of the program.
- it always shows a CONSOLE message



Filename: serial_driver_interface_c.c
Information in this section is obrained from the lab handout made by Karim Naqvi.
- The serial_open function configures and enables the device.
- The send_byte function sends an 8-bit byte to the serial port,
  using the configured bit-rate, # of bits, etc
  Returns 0 on success and non-zero on failure.
- The get_byte function obrains an 8-bit character from the serial port, and returns it.


Filename: serial_input_output.c
- determine_user_input() gets what a user types on the keyboard. It then determines what command was entered
  and directs the program to call the functions required to do the desired command. Most of the function calls
  to other desired commands are listed below.
- print_timer_all() Used in TIME ALL command as well as ADD 32, MUL 32 etc. It prints the  consol line 
  followed by a time value passed to it.
- print_string() Used in almost all commands. prints a string to the console
- TO_UPPER () Converts all ascci values to upper case 
- get_status() Used in STATUS x command. described below.
- on() Used in LED x ON command. described below.
- off() Used in LED x OFF command. described below.
- print_time() Used in TIME command. described below.
- print_date() Used in DATE command. described below.
- restart()
- print_help() Used in HELP command. shows other commands.
//the following were in main but moved here for lab 3
- The TO_UPPER function takes a char data
  and converts it to uppercase.
- The Good_To_Go function takes an integer, i,
  which is the loop counter from the main function,
  prints a new line, and sets i to 100.
- The get_status function takes an integer,
  then determines which light the user
  would like to view the status of using a case statement.
  It returns nothing.
- The on function takes an integer,
  then determines which light the user
  would like to turn on using a case statement.
  It returns nothing.
- The off function takes an integer,
  then determines which light the user
  would like to turn off using a case statement.
  It returns nothing.
- The print_time function prints the time by calling the sendbyte function.
  It returns nothing.
- The print_date function prints the date by calling the sendbyte function.
  It returns nothing
- The Restart function takes a character array and resets
  all of its values to a null character.
  It also resets the GPIOA_CRL register.
  It returns nothing.



Filename: arithmetic_c.c
- generate_random_32() creates two random 16 bit numbers using the rand() function. Shifting one of them
  left by 16 bits and bitwise orring them with each other.
- generate_random_64() calls generate_random_32()  to generatetwo random 32 bit numbers.
  Shifting one of them left by 32 bits and bitwise orring them with each other.
- null_timer() returns the clock cycles it takes to go through a for loop 100 times so be subtracted
  from the following timing functions as a base line.
- add_32_timer() returns the average clock cycles of 100 for loops it takes to go through 
  an add instruction of 2 random 32 bit numbers.
- add_64_timer() returns the average clock cycles of 100 for loops it takes to go through 
  an add instruction of 2 random 64 bit numbers.
- mul_32_timer() returns the average clock cycles of 100 for loops it takes to go through 
  an multiply instruction of 2 random 32 bit numbers.
- mul_64_timer() returns the average clock cycles of 100 for loops it takes to go through 
  an multiply instruction of 2 random 64 bit numbers.
- div_32_timer() returns the average clock cycles of 100 for loops it takes to go through 
  an divide instruction of 2 random 32 bit numbers.
- div_64_timer() returns the average clock cycles of 100 for loops it takes to go through 
  an divide instruction of 2 random 64 bit numbers.
- struct_8_timer() returns the average clock cycles of 100 for loops it takes to go through 
  an equate instruction (struct1 = struct2) of 2 random 8 byte structs.
- struct_128_timer() returns the average clock cycles of 100 for loops it takes to go through 
  an equate instruction (struct1 = struct2) of 2 random 128 byte structs.
- struct_1024_timer() returns the average clock cycles of 100 for loops it takes to go through 
  an equate instruction (struct1 = struct2) of 2 random 1024 byte structs.


Filename: timer_impl_c.c
- timer_init() Enables the clock for TIM2, the auto Reload register set to the maximum 
  clock frequency, thus counter is not blocked. The enable counter and the DIR register sre set to downcount
- timer_start() The TIM2_CNT register is read and returned as a int16_t
- timer_stop() The start time is passed to this function. It reads the current(stop) time. Decides and accounts
  for if the time has wrapped around and return the final time.
- timer_shutdown() undoes all the bits set in timer_init()
- timer_func()is not currently used
- print_timed_value() takes a 16 bit time value and prints a the hex value of that number on the serial port.
- timer_init_with_interrupts() initializes the TIM2 clock and NVIC to be used for an interupt. 


COMPILING
To compile this program, use Keil Tools.