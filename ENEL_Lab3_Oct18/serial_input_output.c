#include <stdint.h>
#include "timer.h"
#include "serial_driver_interface.h"

void determine_user_input(	char data_array[50])
{
		//Initialize the Constant values that will be used for the Help Statement, as well as the console string.
	char CONSOLE[30] = "ENEL487_IS_AWESOME > ";
	char ERROR[40] = "You have entered an invalid command.\0";


	/**
					The Following if statements determine what statement was entered by the user.
					The appropriate functions are called according to the user's entry.
																																											**/
	//If LED # ON was entered
	if (data_array[0] == 'L' && data_array[1] == 'E' && data_array[2] == 'D' && data_array[3] == ' '
		&& data_array[5] == ' ' && data_array[6] == 'O' && data_array[7] == 'N' &&data_array[8] == 0xD)
	{
		on(data_array[4]);
		
	//	j = Good_To_Go(j);
	}

	//If LED # OFF was entered
	else if (data_array[0] == 'L' && data_array[1] == 'E' && data_array[2] == 'D' && data_array[3] == ' '
		&& data_array[5] == ' ' && data_array[6] == 'O' && data_array[7] == 'F' && data_array[8] == 'F'
	&& data_array[9] == 0xD)
	{
		off(data_array[4]);
		
	}
	
	//If ALL ON was entered
	else if (data_array[0] == 'A' && data_array[1] == 'L' && data_array[2] == 'L' && data_array[3] == ' '
		&& data_array[4] == 'O' && data_array[5] == 'N' && data_array[6] == 0xD)
	{
		* regGPIOB_BSRR |= 0x0000FF00;
		
	}
	
	//If ALL OFF was entered
	else if (data_array[0] == 'A' && data_array[1] == 'L' && data_array[2] == 'L' && data_array[3] == ' '
		&& data_array[4] == 'O' && data_array[5] == 'F' && data_array[6] == 'F' && data_array[7] == 0xD)
	{
		* regGPIOB_BSRR |= 0xFF000000;
		
	}
	
	//If STATUS # was entered
	else if (data_array[0] == 'S' && data_array[1] == 'T' && data_array[2] == 'A' && data_array[3] == 'T'
		&& data_array[4] == 'U' && data_array[5] == 'S' && data_array[6] == ' ' && data_array[8] == 0xD)
	{
		get_status(data_array[7]);
		
	}
	
	//If DATE was entered
	else if (data_array[0] == 'D' && data_array[1] == 'A' && data_array[2] == 'T' && data_array[3] == 'E'
		&& data_array[4] == 0xD)
	{
		print_date();			
					
	}
	
	//If TIME was entered
	else if (data_array[0] == 'T' && data_array[1] == 'I' && data_array[2] == 'M' && data_array[3] == 'E'
		&& data_array[4] == 0xD)
	{
		print_time();		
		
	}
	
	//If HELP was entered
	else if (data_array[0] == 'H' && data_array[1] == 'E' && data_array[2] == 'L' && data_array[3] == 'P'
		&& data_array[4] == 0xD)
	{
		print_help();
	}
	
	else if (data_array[0] == 'A' && data_array[1] == 'D' && data_array[2] == 'D' && data_array[3] == ' '
		&& data_array[4] == '3' && data_array[5] == '2' && data_array[6] == 0xD)
	{
		uint32_t timer = 0x00000000;
		
		timer = add_32_timer();

		//Print the console.
		print_string("\n\r");
		print_string(CONSOLE);				
		
		print_timed_value(timer);
	}
	
	else if (data_array[0] == 'A' && data_array[1] == 'D' && data_array[2] == 'D' && data_array[3] == ' '
		&& data_array[4] == '6' && data_array[5] == '4' && data_array[6] == 0xD)
	{
		uint32_t timer = 0x00000000;
		
		timer = add_64_timer();

		//Print the console.
		print_string("\n\r");
		print_string(CONSOLE);				
		
		print_timed_value(timer);				
	}
	
	else if (data_array[0] == 'M' && data_array[1] == 'U' && data_array[2] == 'L' && data_array[3] == ' '
		&& data_array[4] == '3' && data_array[5] == '2' && data_array[6] == 0xD)
	{
		uint32_t timer = 0x00000000;
		
		timer = mul_32_timer();

		//Print the console.
		print_string("\n\r");
		print_string(CONSOLE);				
		
		print_timed_value(timer);
	}
	
	else if (data_array[0] == 'M' && data_array[1] == 'U' && data_array[2] == 'L' && data_array[3] == ' '
		&& data_array[4] == '6' && data_array[5] == '4' && data_array[6] == 0xD)
	{
		uint32_t timer = 0x00000000;
		
		timer = mul_64_timer();

		//Print the console.
		print_string("\n\r");
		print_string(CONSOLE);				
		
		print_timed_value(timer);
	}
	
	else if (data_array[0] == 'D' && data_array[1] == 'I' && data_array[2] == 'V' && data_array[3] == ' '
		&& data_array[4] == '3' && data_array[5] == '2' && data_array[6] == 0xD)
	{
		uint32_t timer = 0x00000000;
		
		timer = div_32_timer();

		//Print the console.
		print_string("\n\r");
		print_string(CONSOLE);				
		
		print_timed_value(timer);
	}
	
	else if (data_array[0] == 'D' && data_array[1] == 'I' && data_array[2] == 'V' && data_array[3] == ' '
		&& data_array[4] == '6' && data_array[5] == '4' && data_array[6] == 0xD)
	{
		uint32_t timer = 0x00000000;
		
		timer = div_64_timer();

		//Print the console.
		print_string("\n\r");
		print_string(CONSOLE);				
		
		print_timed_value(timer);
	}
	
	else if (data_array[0] == 'S' && data_array[1] == 'T' && data_array[2] == 'R' && data_array[3] == 'U'
		&& data_array[4] == 'C' && data_array[5] == 'T' && data_array[6] == ' ' && data_array[7] == '8' 
		&& data_array[8] == 0xD)
	{
		uint32_t timer = 0x00000000;
		
		timer = struct_8_timer();
			
		//Print the console.
		print_string("\n\r");
		print_string(CONSOLE);				
		
		print_timed_value(timer);			
	}
	
	else if (data_array[0] == 'S' && data_array[1] == 'T' && data_array[2] == 'R' && data_array[3] == 'U'
		&& data_array[4] == 'C' && data_array[5] == 'T' && data_array[6] == ' ' && data_array[7] == '1'
		&& data_array[8] == '2' && data_array[9] == '8'	&& data_array[10] == 0xD)
	{
		uint32_t timer = 0x00000000;
		
		timer = struct_128_timer();
			
		//Print the console.
		print_string("\n\r");
		print_string(CONSOLE);				
		
		print_timed_value(timer);
	}
	
	else if (data_array[0] == 'S' && data_array[1] == 'T' && data_array[2] == 'R' && data_array[3] == 'U'
		&& data_array[4] == 'C' && data_array[5] == 'T' && data_array[6] == ' ' && data_array[7] == '1'
		&& data_array[8] == '0' && data_array[9] == '2' && data_array[10] == '4'	&& data_array[11] == 0xD)
	{
		uint32_t timer = 0x00000000;
		
		timer = struct_1024_timer();
			
		//Print the console.
		print_string("\n\r");
		print_string(CONSOLE);				
		
		print_timed_value(timer);
	}
	
	else if (data_array[0] == 'T' && data_array[1] == 'I' && data_array[2] == 'M' && data_array[3] == 'E'
		&& data_array[4] == ' ' && data_array[5] == 'A' && data_array[6] == 'L' && data_array[7] == 'L'
		&& data_array[8] == 0xD)
	{
		uint32_t null_time = (uint32_t)null_timer();
		uint32_t timer_add_32 = (uint32_t)add_32_timer();
		uint32_t timer_add_64 = (uint32_t)add_64_timer();
		uint32_t timer_mul_32 = (uint32_t)mul_32_timer();	
		uint32_t timer_mul_64 = (uint32_t)mul_64_timer();		
		uint32_t timer_div_32 = (uint32_t)div_32_timer();		
		uint32_t timer_div_64 = (uint32_t)div_64_timer();		
		uint32_t timer_struct_8 = (uint32_t)struct_8_timer();		
		uint32_t timer_struct_128 = (uint32_t)struct_128_timer();		
  	uint32_t timer_struct_1024 = (uint32_t)struct_1024_timer();		
			
		print_timer_all(null_time, "Null Time:   ", CONSOLE);
		print_timer_all(timer_add_32, "Add 32:      ", CONSOLE);
		print_timer_all(timer_add_64, "Add 64:      ", CONSOLE);
		print_timer_all(timer_mul_32, "Mul 32:      ", CONSOLE);
		print_timer_all(timer_mul_64, "Mul 64:      ", CONSOLE);
		print_timer_all(timer_div_32, "Div 32:      ", CONSOLE);
		print_timer_all(timer_div_64, "Div 64:      ", CONSOLE);
		print_timer_all(timer_struct_8, "Struct 8:    ", CONSOLE);
		print_timer_all(timer_struct_128, "Struct 128:  ", CONSOLE);
		print_timer_all(timer_struct_1024, "Struct 1024: ", CONSOLE);
	}
	
	else
	{
		print_string("\n\r");
		print_string(ERROR);
	}
	
	return;
}

void print_timer_all(uint32_t timer, char title[], char CONSOLE[30])
{
	print_string("\n\r");
	print_string(CONSOLE);
	print_string(title);				
	print_timed_value(timer);
	
	return;
}

/**
The print_string function takes a const char array,
then prints the array. It returns nothing.
*/
void print_string(char output[])
{
	int i = 0;
	
	while (output[i] != '\0')
	{
		sendbyte(output[i]);
		i++;
	}
	
	return;
}

/**
The TO_UPPER function takes a char data
and converts it to uppercase.
*/
char TO_UPPER (char data)
{
		if (data >=TO_UPPER_LOWER_BOUNDARY && data <= TO_UPPER_UPPER_BOUNDARY)
			data = data - 32;
		
		return data;
}

/**
The get_status function takes an integer,
then determines which light the user
would like to view the status of using a case statement.
It returns nothing.
*/
void get_status(int data)
{
	sendbyte('\n');
	sendbyte('\r');
	
	switch(data)
	{
		case '1':
			if ((* regGPIOB_ODR & 0x00000100) != 0x0)
				sendbyte('1');
			else
				sendbyte('0');
			break;
		case '2':
			if ((* regGPIOB_ODR & 0x00000200) != 0x0)
				sendbyte('1');
			else
				sendbyte('0');
			break;
		case '3':
			if ((* regGPIOB_ODR & 0x00000400) != 0x0)
				sendbyte('1');
			else if ((* regGPIOB_ODR & 0x00000400) == 0x0)
				sendbyte('0');
			break;
		case '4':
			if ((* regGPIOB_ODR & 0x00000800) != 0x0)
				sendbyte('1');
			else
				sendbyte('0');
			break;
		case '5':
			if ((* regGPIOB_ODR & 0x00001000) != 0x0)
				sendbyte('1');
			else
				sendbyte('0');
			break;
		case '6':
			if ((* regGPIOB_ODR & 0x00002000) != 0x0)
				sendbyte('1');
			else
				sendbyte('0');
			break;
		case '7':
			if ((* regGPIOB_ODR & 0x00004000) != 0x0)
				sendbyte('1');
			else
				sendbyte('0');
			break;
		case '8':
			if ((* regGPIOB_ODR & 0x00008000) != 0x0)
				sendbyte('1');
			else
				sendbyte('0');
			break;
		default:
			* regGPIOB_BSRR = 0x0;
			break;
	}	

	return;
}

/**
The on function takes an integer,
then determines which light the user
would like to turn on using a case statement.
It returns nothing.
*/
void on(int data)
{
	switch(data)
	{
		case '1':
			* regGPIOB_BSRR |= INITIAL_LIGHTS;
			break;
		case '2':
			* regGPIOB_BSRR |= INITIAL_LIGHTS << 1;
			break;
		case '3':
			* regGPIOB_BSRR |= INITIAL_LIGHTS << 2;
			break;
		case '4':
			* regGPIOB_BSRR |= INITIAL_LIGHTS << 3;
			break;
		case '5':
			* regGPIOB_BSRR |= INITIAL_LIGHTS << 4;
			break;
		case '6':
			* regGPIOB_BSRR |= INITIAL_LIGHTS << 5;
			break;
		case '7':
			* regGPIOB_BSRR |= INITIAL_LIGHTS << 6;
			break;
		case '8':
			* regGPIOB_BSRR |= INITIAL_LIGHTS << 7;
			break;
		default:
			* regGPIOB_BSRR = 0x0;
			break;
	}
	
	return;
}

/**
The off function takes an integer,
then determines which light the user
would like to turn off using a case statement.
It returns nothing.
*/
void off(int data)
{
	switch(data)
	{
		case '1':
			* regGPIOB_BSRR |= (uint32_t) RESET_LIGHTS;
			break;
		case '2':
			* regGPIOB_BSRR |= (uint32_t) RESET_LIGHTS << 1;
			break;
		case '3':
			* regGPIOB_BSRR |= (uint32_t) RESET_LIGHTS << 2;
			break;
		case '4':
			* regGPIOB_BSRR |= (uint32_t) RESET_LIGHTS << 3;
			break;
		case '5':
			* regGPIOB_BSRR |= (uint32_t) RESET_LIGHTS << 4;
			break;
		case '6':
			* regGPIOB_BSRR |= (uint32_t) RESET_LIGHTS << 5;
			break;
		case '7':
			* regGPIOB_BSRR |= (uint32_t) RESET_LIGHTS << 6;
			break;
		case '8':
			* regGPIOB_BSRR |= (uint32_t) RESET_LIGHTS << 7;
			break;
	default:
			* regGPIOB_BSRR = 0x0;
			break;
	}
	
	return;
}

/**
The print_time function prints the time by calling the sendbyte function.
It returns nothing.
*/
void print_time()
{
	//Declare a loop counting variable.
	int i = 0;
	
	//Print a new line
	sendbyte('\n');
	sendbyte('\r');
	sendbyte(' ');
	
	//Print the time
	for (i = 0; i < 11; i++)
		sendbyte(__TIME__[i]);
	
	return;
}

/**
The print_date function prints the date by calling the sendbyte function.
It returns nothing.
*/
void print_date()
{
	//Declare a loop counting variable.
	int i = 0;
	
	//Print a new line
	sendbyte('\n');
	sendbyte('\r');
	sendbyte(' ');
	
	//Print the date
	for (i = 0; i < 11; i++)
		sendbyte(__DATE__[i]);
	
	return;
}


/**
The Restart function takes a character array and resets
all of its values to a null character.
It also resets the GPIOA_CRL register.
It returns nothing.
*/
void restart(char data_array[50])
{
	//Declare a loop counting variable.
	int i = 0;
	
	//Reset the data_array.
	for (i = 0; i < 50; i++)
		data_array[i] = '\0';
	
	//Reset the GPIOA_CRL register
		* regGPIOA_CRL = 0x44444B44;
	
	return;
}

void print_help(void)
{
	char HELP_1[60] = "- To turn on an LED, enter the following command,\0";
	char HELP_2[16] ="\n\r    LED # ON\0";
	char HELP_3[60] = "- To turn off an LED, enter the following command,\0";
	char HELP_4[17] ="\n\r    LED # OFF\0";
	char HELP_5[70] = " - To check the status of an LED, enter the following command:\0";
	char HELP_6[16] ="\n\r    STATUS #\0";
	char HELP_7[60] = " - To turn on all LEDs, enter the following command:\0";
	char HELP_8[14] ="\n\r    ALL ON\0";	
	char HELP_9[60] = " - To turn all off LEDs, enter the following command:\0";
	char HELP_10[15] ="\n\r    ALL OFF\0";
	char HELP_11[60] = " - To display the time, enter the following command:\0";
	char HELP_12[12] ="\n\r    TIME\0";
	char HELP_13[60] = " - To display the date, enter the following command:\0";
	char HELP_14[12] ="\n\r    DATE\0";
	char HELP_15[60] = "\n\r  where # is the number of the LED you want to turn on.\0";
	
	print_string("\n\r");
	print_string(HELP_1);
	print_string(HELP_2);
	print_string("\n\r");
	print_string(HELP_3);
	print_string(HELP_4);
	print_string("\n\r");
	print_string(HELP_5);
	print_string(HELP_6);
	print_string("\n\r");
	print_string(HELP_7);
	print_string(HELP_8);
	print_string("\n\r");
	print_string(HELP_9);
	print_string(HELP_10);
	print_string("\n\r");
	print_string(HELP_11);
	print_string(HELP_12);
	print_string("\n\r");
	print_string(HELP_13);
	print_string(HELP_14);
	print_string("\n\r");
	print_string(HELP_15);
	print_string("\n\r");
}
